//
//  ImageGalleryViewController.swift
//  PhotoGallery
//
//  Created by Michael Miller on 10/8/17.
//  Copyright © 2017 mikemiller.design. All rights reserved.
//

import UIKit

class ImageGalleryViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    // MARK: Outlets
    @IBOutlet weak var navigationBar: PGNavigationBar!
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: Variables
    let collectionViewMargin:CGFloat = 15
    let collectionViewGutter:CGFloat = 8
    
    // Photo Dataset
    /*
     Normally, this would probably be a list of objects containing an image url that is parsed from JSON.
     If that were the case, I would download these images in cellForRowAt and cache them for quick access
     */
    let photos:[Photo] = [
        Photo(title: "Lounge",         image: Images.Lounge),
        Photo(title: "Mountain View",  image: Images.MountainView),
        Photo(title: "Air",            image: Images.Air),
        Photo(title: "Racecar",        image: Images.Racecar),
        Photo(title: "Desks",          image: Images.Desks),
        Photo(title: "Another Lounge", image: Images.Lounge2),
        Photo(title: "Hive",           image: Images.Hive),
        Photo(title: "Logo",           image: Images.Logo),
        Photo(title: "Lounge",         image: Images.Lounge),
        Photo(title: "Mountain View",  image: Images.MountainView),
        Photo(title: "Air",            image: Images.Air),
        Photo(title: "Racecar",        image: Images.Racecar),
        Photo(title: "Desks",          image: Images.Desks),
        Photo(title: "Another Lounge", image: Images.Lounge2),
        Photo(title: "Hive",           image: Images.Hive),
        Photo(title: "Logo",           image: Images.Logo)
        ]
    
    // MARK: View Delegates
    override func viewDidLoad() {
        super.viewDidLoad()

        // Init CollectionView
        setupCollectionView()
    }
    
    // MARK: Collection View
    fileprivate func setupCollectionView() {
        collectionView.register(UINib(nibName: ImageGalleryCollectionViewCell.id, bundle: nil), forCellWithReuseIdentifier: ImageGalleryCollectionViewCell.id)
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    // MARK: Collection View Flow Layout Delegate
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return collectionViewGutter
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return collectionViewGutter
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        // Dimension values
        let numberOfColumns:CGFloat = 2
        let leftRightMargins:CGFloat = collectionViewMargin * 2
        let heightRatio:CGFloat = 1.4 // This number was measured based on the design
        
        // Get non-cell space
        let nonCellSpace = leftRightMargins + collectionViewGutter
        
        // Return proper cell size
        let itemWidth = (collectionView.bounds.width - nonCellSpace) / numberOfColumns
        return CGSize(width: itemWidth, height: itemWidth / heightRatio)
    }
    
    // MARK: Collection View Delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageGalleryCollectionViewCell.id, for: indexPath) as! ImageGalleryCollectionViewCell
        cell.imageView.image = photos[indexPath.row].image
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        // Launch ImageDetailViewController
        // Done this way rather than with a view to allow the ability to work with safeareas and a fresh xib
        let imageDetailViewController = ImageDetailViewController()
        imageDetailViewController.modalPresentationStyle = .overCurrentContext
        imageDetailViewController.imageGalleryViewController = self
        imageDetailViewController.originalIndexPath = indexPath
        
        // Get current selected cell & it's frame
        let selectedCell = collectionView.cellForItem(at: indexPath)
        if let selectedCellFrame = selectedCell!.superview?.convert(selectedCell!.frame, to: nil) {
            imageDetailViewController.originalSharedElementFrame = selectedCellFrame
        }
        
        // Show the detail view
        present(imageDetailViewController, animated: false, completion: nil)
    }

}
