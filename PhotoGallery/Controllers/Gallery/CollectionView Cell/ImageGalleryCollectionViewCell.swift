//
//  ImageGalleryCollectionViewCell.swift
//  PhotoGallery
//
//  Created by Michael Miller on 10/9/17.
//  Copyright © 2017 mikemiller.design. All rights reserved.
//

import UIKit

class ImageGalleryCollectionViewCell: UICollectionViewCell {
    
    // MARK: Variables
    static let id = "ImageGalleryCollectionViewCell"

    // MARK: Outlets
    @IBOutlet weak var imageView: UIImageView!
    
    // MARK: View Init
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Add border to the view
        imageView.addBorder()
    }

}
