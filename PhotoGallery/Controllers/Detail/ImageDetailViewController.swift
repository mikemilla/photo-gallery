//
//  ImageDetailViewController.swift
//  PhotoGallery
//
//  Created by Michael Miller on 10/10/17.
//  Copyright © 2017 mikemiller.design. All rights reserved.
//

import UIKit
import AVFoundation

class ImageDetailViewController: UIViewController, UIScrollViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, PGSharedElementGestureInteractorDelegate {

    // MARK: Outlets
    @IBOutlet weak var navigationBar: PGNavigationBar!
    @IBOutlet weak var collectionView: PGPagingCollectionView!
    @IBOutlet weak var actionBar: PGActionBar!
    
    // MARK: Data
    fileprivate var pagingIndex = 0
    fileprivate var photos:[Photo] = []
    var imageGalleryViewController:ImageGalleryViewController! {
        didSet {
            photos = imageGalleryViewController.photos
        }
    }
    
    // MARK: Views
    fileprivate var tempNavigationBar: PGNavigationBar!
    
    // MARK: Shared Element Properties
    fileprivate var sharedElementGestureInteractor:PGSharedElementGestureInteractor!
    var sharedElementView: PGSharedElementView!
    var originalSharedElementFrame: CGRect!
    var originalIndexPath: IndexPath!
    
    // MARK: Animation
    fileprivate let animationDurationEnter:Double = 0.26
    fileprivate let animationDurationExit:Double = 0.26
    fileprivate let animationDurationReset:Double = 0.22
    fileprivate let animationDurationBarFade:Double = 0.28
    
    // MARK: View Delegates
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup rest of view
        setupBars()
        setupCollectionView()
        setupGestures()
        
        // Create shared and give it some data
        setupSharedElement(photo: photos[originalIndexPath.row],
                           frame: originalSharedElementFrame,
                           contentMode: .scaleAspectFill)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Perform shared element transition
        animateEnter()
    }
    
    // MARK: Bars
    fileprivate func setupBars() {
        
        // Set navigation bar title
        navigationBar.title = photos[originalIndexPath.row].title
        
        // Set styles
        navigationBar.alpha = 0
        actionBar.alpha = 0
        
        // Close the view controller
        navigationBar.closeButtonAction = {
            self.hidePreviousViewCell()
            self.animateExit(flingVelocity: 0, rotation: 0)
        }
    }
    
    // MARK: Collection View
    fileprivate func setupCollectionView() {
        collectionView.gutterBetweenPages = imageGalleryViewController.collectionViewGutter
        collectionView.register(UINib(nibName: ImageDetailCollectionViewCell.id, bundle: nil), forCellWithReuseIdentifier: ImageDetailCollectionViewCell.id)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.isHidden = true
    }
    
    // MARK: Gestures
    fileprivate func setupGestures() {
        sharedElementGestureInteractor = PGSharedElementGestureInteractor(imageDetailViewController: self)
        sharedElementGestureInteractor.delegate = self
    }
    
    // MARK: Shared Element Functions
    fileprivate func animateEnter() {
        
        // Jump to the current selected item while collection view is hidden
        collectionView.scrollToItem(at: originalIndexPath, at: .centeredHorizontally, animated: false)
        
        // Set background
        view.backgroundColor = UIColor.black.withAlphaComponent(0)
        
        // Add a temparary navigation bar
        // This gives the illusion that the sharedElementView is behind the navigation bar in the previous view
        if let previousViewControllerNavigationBar = imageGalleryViewController.navigationBar {
            tempNavigationBar = PGNavigationBar(frame: previousViewControllerNavigationBar.frame)
            tempNavigationBar.title = previousViewControllerNavigationBar.title
            view.insertSubview(tempNavigationBar, aboveSubview: sharedElementView)
        }
        
        // Hide background cell
        hidePreviousViewCell()
        
        // Create shared element transition
        let sharedElementTransition = PGSharedElementTransition(
            sharedElement: sharedElementView,
            startFrame: originalSharedElementFrame,
            endFrame: collectionView.frame,
            animationDuration: animationDurationEnter)
        
        // Extras
        sharedElementTransition.shouldDrawAnimationLine = userDefaults.bool(forKey: Keys.ShowAnimationPaths)

        // Listen to ending
        sharedElementTransition.onTransitonEnd = {

            // Show background cell
            self.showPreviousViewCell()
            
            // Create shared element image view
            self.setupSharedElement(photo: self.photos[self.originalIndexPath.row], frame: self.collectionView.frame, contentMode: .scaleAspectFit)
            
            // Toggle view visibility
            self.hideSharedElement()
        }

        // Start Animation
        sharedElementTransition.start()

        // Animation bars and bars
        UIView.animate(withDuration: animationDurationEnter) {
            self.view.backgroundColor = UIColor.black.withAlphaComponent(1)
            self.navigationBar.alpha = 1
            self.actionBar.alpha = 1
        }
    }
    
    fileprivate func animateExit(flingVelocity: CGFloat, rotation: CGFloat) {
        
        // Capture current frame / origin of the collection view cell imageView if zoomed in
        if let currentCell = self.collectionView.cellForItem(at: self.originalIndexPath) as? ImageDetailCollectionViewCell {
            if (currentCell.scrollView.zoomScale > 1) {
                
                // Get the current origin of the zoomed cells imageView in the view controller view
                let zoomedOriginInView = currentCell.imageView.convert(currentCell.imageView.frame.origin, to: self.view)
                
                // Set frame of exact zoomed position
                sharedElementView.frame = currentCell.imageView.frame
                sharedElementView.frame.origin = zoomedOriginInView
            }
        }
        
        // Get frame for collection view cell we need to animate to (Supports offscreen cells! 🤘)
        let sharedElementAttributes = imageGalleryViewController.collectionView.layoutAttributesForItem(at: originalIndexPath)
        let sharedElementFrame = imageGalleryViewController.collectionView.convert(sharedElementAttributes!.frame, to: nil)
        
        // Show shared element
        showSharedElement()
        
        // Create shared element transition
        let sharedElementTransition = PGSharedElementTransition(
            sharedElement: sharedElementView,
            startFrame: sharedElementView.frame,
            endFrame: sharedElementFrame,
            animationDuration: animationDurationExit)
        
        // Extras
        sharedElementTransition.isDismissing = true
        sharedElementTransition.flingVelocity = flingVelocity
        sharedElementTransition.currentRotation = rotation
        sharedElementTransition.shouldDrawAnimationLine = userDefaults.bool(forKey: Keys.ShowAnimationPaths)
        
        // Listen to ending
        sharedElementTransition.onTransitonEnd = {
            
            // Show background cell
            self.showPreviousViewCell()
            
            // Dismiss this view controller
            self.dismiss(animated: false, completion: nil)
        }
        
        // Start Animation
        sharedElementTransition.start()
        
        // Animation bars and bars
        UIView.animate(withDuration: animationDurationEnter) {
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0)
            self.navigationBar.alpha = 0
            self.actionBar.alpha = 0
        }
    }
    
    fileprivate func animateReset() {
        
        // Animate reset
        UIView.animate(withDuration: animationDurationReset, animations: {
            
            // Reset Shared element
            self.sharedElementView.transform = CGAffineTransform.identity
            self.sharedElementView.frame = self.collectionView.frame
            
            // Reset alpha adjusted views
            self.translatePositionTricks(position: 0, maxPosition: 1, shouldScale: false)
            
        }) { (didComplete) in
            self.hideSharedElement()
            self.showPreviousViewCell()
        }
    }
    
    // MARK: Previous View Cell
    fileprivate func hidePreviousViewCell() {
        let sharedCellToUpdate = imageGalleryViewController.collectionView.cellForItem(at: originalIndexPath)
        if let sharedCell = sharedCellToUpdate {
            sharedCell.isHidden = true
        }
    }
    
    fileprivate func showPreviousViewCell() {
        let sharedCellToUpdate = imageGalleryViewController.collectionView.cellForItem(at: originalIndexPath)
        if let sharedCell = sharedCellToUpdate {
            sharedCell.isHidden = false
        }
    }
    
    // MARK: Shared Element
    fileprivate func setupSharedElement(photo: Photo, frame: CGRect, contentMode: UIViewContentMode) {
        
        // Remove the view if it exists
        if (sharedElementView != nil && view.subviews.contains(sharedElementView)) {
            sharedElementView.removeFromSuperview()
        }
        
        // Setup the element
        sharedElementView = PGSharedElementView(frame: frame)
        sharedElementView.image = photo.image
        sharedElementView.contentMode = contentMode
        view.insertSubview(sharedElementView, aboveSubview: collectionView)
    }
    
    fileprivate func showSharedElement() {
        
        // Ensure view is hidden
        if (!sharedElementView.isHidden) {
            return
        }
        
        // Toggle view
        collectionView.isHidden = true
        sharedElementView.isHidden = false
        sharedElementView.layer.masksToBounds = false
        
        // Sets image if not set
        if (sharedElementView.image != photos[originalIndexPath.row].image) {
            sharedElementView.image = photos[originalIndexPath.row].image
        }
    }
    
    fileprivate func hideSharedElement() {
        
        // Ensure view is hidden
        if (sharedElementView.isHidden) {
            return
        }
        
        // Toggle views
        collectionView.isHidden = false
        sharedElementView.isHidden = true
    }
    
    fileprivate func showBars() {
        
        // Setup
        navigationBar.alpha = 0
        navigationBar.isHidden = false
        actionBar.isHidden = false
        tempNavigationBar.isHidden = true
        
        // Animate
        UIView.animate(withDuration: animationDurationBarFade, animations: {
            self.navigationBar.alpha = 1
            self.actionBar.alpha = 1
        }) { (finished) in
            self.tempNavigationBar.isHidden = !finished
        }
    }
    
    fileprivate func hideBars() {
        
        // Setup
        tempNavigationBar.isHidden = true
        navigationBar.alpha = 1
        actionBar.alpha = 1
        
        // Animate
        UIView.animate(withDuration: animationDurationBarFade, animations: {
            self.navigationBar.alpha = 0
            self.actionBar.alpha = 0
        }) { (finished) in
            self.navigationBar.isHidden = finished
            self.actionBar.isHidden = finished
        }
    }
    
    // MARK: Translation Trick
    fileprivate func translatePositionTricks(position: CGFloat, maxPosition: CGFloat, shouldScale: Bool) {
        
        // Ensure the view is visible
        if (!sharedElementView.isHidden) {
            let valueForTranslation = 1 - (position * (1 / maxPosition))
            
            // Translate views
            navigationBar.alpha = valueForTranslation
            actionBar.alpha = valueForTranslation
            view.backgroundColor = UIColor.black.withAlphaComponent(valueForTranslation)
            
            // Translate scale
            if (shouldScale) {
                let max:CGFloat = 0.8
                let translatableScale:CGFloat = (1 - max) * valueForTranslation
                if (max + translatableScale > max) {
                    sharedElementView.transform = CGAffineTransform(scaleX: max + translatableScale, y: max + translatableScale)
                }
            }
        }
    }
    
    // MARK: Shared Element Panning
    func didBeginPaging(sharedElement: UIView) {
        hideSharedElement()
        showPreviousViewCell()
    }
    
    func didBeginDragging(sharedElement: UIView) {
        hidePreviousViewCell()
    }
    
    func isDragging(sharedElement: UIView, translationPosition: CGFloat, distance: CGFloat) {
        showSharedElement()
        translatePositionTricks(position: translationPosition, maxPosition: translationPosition > 0 ? distance : -distance, shouldScale: true)
    }
    
    func didEndDragging(sharedElement: UIView, isDismissable: Bool, velocity: CGFloat) {
        isDismissable ? animateExit(flingVelocity: velocity, rotation: 0) : animateReset()
    }
    
    // MARK: Shared Element Pinching
    func didBeginPinching(sharedElement: UIView) {
        showSharedElement()
        hidePreviousViewCell()
    }
    
    func isPinching(sharedElement: UIView, scale: CGFloat, rotation: CGFloat) {
        translatePositionTricks(position: 1 - scale, maxPosition: 0.5, shouldScale: false)
    }
    
    func didEndPinching(sharedElement: UIView, isDismissable: Bool, scale: CGFloat, rotation: CGFloat) {
        
        // Check dismissing threshold
        if (isDismissable && !sharedElement.isHidden) {
            animateExit(flingVelocity: 0, rotation: rotation)
        } else {
            
            // Force reset zoomed cell
            if let currentCell = collectionView.cellForItem(at: originalIndexPath) as? ImageDetailCollectionViewCell {
                currentCell.scrollView.setZoomScale(1, animated: true)
            }
            
            // Reset the shared element
            animateReset()
        }
    }
    
    // MARK: Shared Element Tap
    func didTap(sharedElement: UIView) {
        if (navigationBar.isHidden) {
            showBars()
        } else {
            hideBars()
        }
    }
    
    // MARK: Collection View Delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageDetailCollectionViewCell.id, for: indexPath) as! ImageDetailCollectionViewCell
        cell.panGestureRef = sharedElementGestureInteractor.panGestureWithOneFinger
        cell.singleTapGestureRef = sharedElementGestureInteractor.tapGesture
        cell.imageView.image = photos[indexPath.row].image
        cell.scrollView.contentSize = collectionView.frame.size
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        // Reset zoom on cell
        // This prevents the collectionView from reusing the cell's scrollView.zoomScale on other cell
        if let currentCell = cell as? ImageDetailCollectionViewCell {
            currentCell.scrollView.setZoomScale(1, animated: false)
        }
    }
    
    // MARK: ScrollView Delegates
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        // Ensure first load doesn't call this
        if (scrollView.isTracking || scrollView.isDragging || scrollView.isDecelerating) {
            
            // Tell collection view to page
            collectionView.isPaging()
            
            // Save index and set title using offset page index
            originalIndexPath.row = collectionView.offsetPageIndex
            navigationBar.title = photos[originalIndexPath.row].title
        }
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        // Turn all gestures on
        // This allows a user to flick to dismiss while the pager is scrolling (Feels more natural)
        sharedElementGestureInteractor.enableAllGestures()

        // Handle paging off collection view
        collectionView.willEndPaging(withVelocity: velocity,
                                     targetContentOffset: targetContentOffset,
                                     newPageIndex: originalIndexPath.row)
    }
    
    // MARK: Action Bar / Show Animation Paths
    let userDefaults = UserDefaults.standard
    @IBAction func actionBarButtonClick(_ sender: Any) {
        
        // Toggle show paths
        var title:String = ""
        if let _ = userDefaults.object(forKey: Keys.ShowAnimationPaths) {
            let showPaths = userDefaults.bool(forKey: Keys.ShowAnimationPaths)
            userDefaults.set(!showPaths, forKey: Keys.ShowAnimationPaths)
            title = userDefaults.bool(forKey: Keys.ShowAnimationPaths) ? "Animation Paths Shown" : "Animation Paths Hidden"
        } else {
            userDefaults.set(true, forKey: Keys.ShowAnimationPaths)
            title = "Animation Paths Shown"
        }
        
        // Show alert
        let alert = UIAlertController(title: title, message: nil, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

}
