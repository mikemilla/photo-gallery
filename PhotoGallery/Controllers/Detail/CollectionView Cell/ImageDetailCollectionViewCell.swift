//
//  ImageDetailCollectionViewCell.swift
//  PhotoGallery
//
//  Created by Michael Miller on 10/10/17.
//  Copyright © 2017 mikemiller.design. All rights reserved.
//

import UIKit

class ImageDetailCollectionViewCell: UICollectionViewCell, UIScrollViewDelegate {
    
    // MARK: Variables
    static let id = "ImageDetailCollectionViewCell"
    var doubleTapGesture:UITapGestureRecognizer!
    var panGestureRef:UIPanGestureRecognizer?
    var singleTapGestureRef:UITapGestureRecognizer? {
        didSet {
            
            // Allow double tap to override single tap 
            singleTapGestureRef!.require(toFail: doubleTapGesture)
        }
    }

    // MARK: Outlets
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    // MARK: View Init
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Handle pinches and zooms
        scrollView.delegate = self
        scrollView.minimumZoomScale = 1
        scrollView.maximumZoomScale = 4
        scrollView.zoomScale = 1
        scrollView.decelerationRate = UIScrollViewDecelerationRateFast
        
        // Set double tap gesture
        doubleTapGesture = UITapGestureRecognizer(target: self, action: #selector(imageViewDoubleTapGesture(_ :)))
        doubleTapGesture.numberOfTapsRequired = 2
        scrollView.addGestureRecognizer(doubleTapGesture)
    }
    
    // MARK: Double Tap Gesture
    @objc func imageViewDoubleTapGesture(_ sender: UITapGestureRecognizer) {
        if (scrollView.zoomScale == 1) {
            scrollView.zoom(to: zoomRectForScale(scale: scrollView.maximumZoomScale, center: sender.location(in: sender.view)), animated: true)
        } else {
            scrollView.setZoomScale(1, animated: true)
        }
    }
    
    // Gets a CGRect of where the user double tapped
    fileprivate func zoomRectForScale(scale: CGFloat, center: CGPoint) -> CGRect {
        var zoomRect = CGRect.zero
        zoomRect.size.height = imageView.frame.size.height / scale
        zoomRect.size.width = imageView.frame.size.width  / scale
        let newCenter = imageView.convert(center, from: scrollView)
        zoomRect.origin.x = newCenter.x - (zoomRect.size.width / 2.0)
        zoomRect.origin.y = newCenter.y - (zoomRect.size.height / 2.0)
        return zoomRect
    }
    
    // MARK: ScrollView Delegate
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        panGestureRef?.isEnabled = scrollView.zoomScale == 1
    }

}
