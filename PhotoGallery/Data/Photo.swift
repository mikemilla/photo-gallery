//
//  Photo.swift
//  PhotoGallery
//
//  Created by Michael Miller on 10/8/17.
//  Copyright © 2017 mikemiller.design. All rights reserved.
//

import UIKit

class Photo {
    var title:String!
    var image:UIImage!
    
    init(title: String, image: UIImage) {
        self.title = title
        self.image = image
    }
}
