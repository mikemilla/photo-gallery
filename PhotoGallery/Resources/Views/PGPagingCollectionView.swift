//
//  PGPagingCollectionVIew.swift
//  PhotoGallery
//
//  Created by Michael Miller on 10/16/17.
//  Copyright © 2017 mikemiller.design. All rights reserved.
//

import UIKit

@IBDesignable class PGPagingCollectionView: UICollectionView {
    
    // MARK: Variables
    var currentPageIndex = 0
    var offsetPageIndex = 0
    var didLayoutSubviews = false
    
    // MARK: Inspectables
    @IBInspectable var gutterBetweenPages:CGFloat = 8
    
    // MARK: Layout Subviews
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // Ensure this is called only once
        if (!didLayoutSubviews) {
            didLayoutSubviews = true
            setupLayout()
        }
    }
    
    // MARK: Setup Flow Layout
    fileprivate func setupLayout() {
        
        // Set page transition ending rate
        decelerationRate = 0.01
        
        // Paging is handled in the functions below 🙈
        isPagingEnabled = false
        
        // Set layout
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = .zero
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = gutterBetweenPages
        layout.itemSize = frame.size
        collectionViewLayout = layout
    }
    
    // MARK: Paging Functionality
    func isPaging() {
        
        // Ensure the selected index path is accurate to the nearest page
        let width = frame.width
        let pageIndex = Int(contentOffset.x / width)
        let pageOffset = contentOffset.x - CGFloat(gutterBetweenPages * CGFloat(pageIndex))
        
        // Saves a value that the targetContentOffset uses to get the correct page
        offsetPageIndex = Int((pageOffset + (0.5 * width)) / width)
        currentPageIndex = Int(pageOffset / width)
    }
    
    func willEndPaging(withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>, newPageIndex: Int) {
        
        // Get correct paging positions
        let pageWidth = CGFloat(frame.width + gutterBetweenPages)
        let targetXContentOffset = CGFloat(targetContentOffset.pointee.x)
        let contentWidth = CGFloat(contentSize.width)
        var newPage = CGFloat(newPageIndex)
        
        // Handle velocity scroll or paging scroll
        if (velocity.x == 0) {
            newPage = floor((targetXContentOffset - CGFloat(pageWidth) / 2) / CGFloat(pageWidth)) + 1.0
        } else {
            newPage = CGFloat(velocity.x > 0 ? currentPageIndex + 1 : currentPageIndex)
            if newPage < 0 {
                newPage = 0
            } else if (newPage > contentWidth / pageWidth) {
                newPage = ceil(contentWidth / pageWidth) - 1.0
            }
        }
        
        // Set correct scroll point
        let point = CGPoint (x: CGFloat(newPage * pageWidth), y: targetContentOffset.pointee.y)
        targetContentOffset.pointee = point
    }

}
