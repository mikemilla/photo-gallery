//
//  NavBar.swift
//  PhotoGallery
//
//  Created by Michael Miller on 10/8/17.
//  Copyright © 2017 mikemiller.design. All rights reserved.
//

import UIKit

@IBDesignable class PGNavigationBar: UIView {
    
    // MARK: Closures
    var closeButtonAction: (() -> Void)?
    
    // MARK: Variables
    fileprivate var titleLabel:UILabel = UILabel()
    fileprivate var backgroundView:UIView = UIView()
    fileprivate var backgroundViewStroke:CALayer = CALayer()
    fileprivate var closeButton:UIButton = UIButton()
    
    // MARK: Inspectables
    @IBInspectable var isLightTheme:Bool = true
    @IBInspectable var title:String? {
        didSet {
            if (titleLabel.text != title) {
                titleLabel.text = title
            }
        }
    }
    
    // MARK: View Layout
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // Remove all views
        // This will get called when the device is rotated
        for view in subviews {
            view.removeFromSuperview()
        }
        
        // Add background view
        backgroundView = UIView(frame: frame)
        backgroundView.frame.size.height = frame.size.height * 2
        backgroundView.frame.origin.y = -backgroundView.frame.size.height / 2
        backgroundView.backgroundColor = isLightTheme ? Colors.NavigationBarLight : Colors.NavigationBarDark
        backgroundView.clipsToBounds = true
        addSubview(backgroundView)
        
        // Add stroke to bottom of view
        let strokeHeight:CGFloat = 1
        backgroundViewStroke.frame = CGRect.init(x: 0, y: backgroundView.frame.height - strokeHeight, width: backgroundView.frame.width, height: strokeHeight)
        backgroundViewStroke.backgroundColor = isLightTheme ? Colors.NavigationBarStroke.cgColor : UIColor.clear.cgColor
        backgroundView.layer.addSublayer(backgroundViewStroke)
        
        // Add title
        titleLabel = UILabel(frame: frame)
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont(name: Fonts.HelveticaNeueMedium, size: 20)!
        titleLabel.textColor = isLightTheme ? Colors.TitleDark : Colors.TitleLight
        titleLabel.frame.origin.y = 0
        titleLabel.text = title
        addSubview(titleLabel)
        
        // Add close button
        if (!isLightTheme) {
            let buttonSizeDimension = frame.size.height
            closeButton = UIButton(frame: CGRect(x: frame.size.width - buttonSizeDimension, y: 0, width: buttonSizeDimension, height: buttonSizeDimension))
            closeButton.addTarget(self, action: #selector(closeButtonPressed), for: .touchUpInside)
            closeButton.setImage(Icons.Close, for: .normal)
            addSubview(closeButton)
        }
    }
    
    // MARK: Button Action
    @objc private func closeButtonPressed(sender: UIButton) {
        if let closeButtonAction = self.closeButtonAction {
            closeButtonAction()
        }
    }

}
