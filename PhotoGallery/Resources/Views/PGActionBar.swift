//
//  PGActionBar.swift
//  PhotoGallery
//
//  Created by Michael Miller on 10/13/17.
//  Copyright © 2017 mikemiller.design. All rights reserved.
//

import UIKit

@IBDesignable class PGActionBar: UIView {
    
    // MARK: Variables
    fileprivate var backgroundView:UIView = UIView()
    
    // MARK: View Layout
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // Remove background view if it exists
        if (subviews.contains(backgroundView)) {
            backgroundView.removeFromSuperview()
        }
        
        // Add background view
        backgroundView = UIView(frame: frame)
        backgroundView.frame.size.height = frame.size.height * 2
        backgroundView.frame.origin.y = 0
        backgroundView.backgroundColor = Colors.NavigationBarDark
        addSubview(backgroundView)
        sendSubview(toBack: backgroundView)
    }

}
