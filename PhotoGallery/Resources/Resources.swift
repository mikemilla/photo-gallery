//
//  Resources.swift
//  PhotoGallery
//
//  Created by Michael Miller on 10/8/17.
//  Copyright © 2017 mikemiller.design. All rights reserved.
//

import Foundation
import UIKit

// MARK: Fonts
struct Fonts {
    static let HelveticaNeueMedium:String = "HelveticaNeue-Medium"
}

// MARK: Colors
struct Colors {
    static let TitleDark:UIColor = UIColor.init(hex: "4a4a4a")
    static let TitleLight:UIColor = UIColor.init(hex: "ebebeb")
    static let NavigationBarLight:UIColor = UIColor.init(hex: "f5f5f5")
    static let NavigationBarDark:UIColor = UIColor.init(hex: "333333")
    static let NavigationBarStroke:UIColor = UIColor.init(hex: "e3e3e3")
}

// MARK: Icons
struct Icons {
    static let Close:UIImage = UIImage(named: "icon_close")!
}

// MARK: Images
struct Images {
    static let Lounge:UIImage = UIImage(named: "lounge")!
    static let MountainView:UIImage = UIImage(named: "mountainview")!
    static let Air:UIImage = UIImage(named: "air")!
    static let Racecar:UIImage = UIImage(named: "racecar")!
    static let Desks:UIImage = UIImage(named: "desks")!
    static let Lounge2:UIImage = UIImage(named: "lounge2")!
    static let Hive:UIImage = UIImage(named: "hive")!
    static let Logo:UIImage = UIImage(named: "logo")!
}

// MARK: Keys
struct Keys {
    static let ShowAnimationPaths:String = "show_animation_paths"
}
