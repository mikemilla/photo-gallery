//
//  Extensions.swift
//  PhotoGallery
//
//  Created by Michael Miller on 10/8/17.
//  Copyright © 2017 mikemiller.design. All rights reserved.
//

import Foundation
import UIKit

// MARK: UIColor
extension UIColor {
    
    // Allows us to pass Hex Code to UIColor
    // # is included
    convenience init(hex: String) {
        
        // String -> UInt32
        var rgbValue: UInt32 = 0
        Scanner(string: hex).scanHexInt32(&rgbValue)
        
        // UInt32 -> R,G,B
        let red = CGFloat((rgbValue >> 16) & 0xff) / 255.0
        let green = CGFloat((rgbValue >> 08) & 0xff) / 255.0
        let blue = CGFloat((rgbValue >> 00) & 0xff) / 255.0
        
        self.init(red: red, green: green, blue: blue, alpha: 1.0)
    }
    
}

// MARK: CGRect
extension CGRect {
    
    // Gets the center point of a frame
    func calculateCenter() -> CGPoint {
        return CGPoint(
            x: self.origin.x + (self.width / 2),
            y: self.origin.y + (self.height / 2))
    }
}

// MARK: ImageView
extension UIImageView {
    
    // Gets the scaled size of the image in the imageView
    func sizeOfImageInView() -> CGSize {
        if let img = self.image {
            let imageRatio = img.size.width / img.size.height
            let viewRatio = self.frame.size.width / self.frame.size.height
            if (imageRatio < viewRatio) {
                let scale = self.frame.size.height / img.size.height
                let width = scale * img.size.width
                return CGSize(width: width, height: self.frame.size.height)
            } else {
                let scale = self.frame.size.width / img.size.width
                let height = scale * img.size.height
                return CGSize(width: self.frame.size.width, height: height)
            }
        }
        return .zero
    }
    
    // Returns the frame of an image for a new frame that would contain it
    // Allows for scaling animations that don't change content mode
    func frameForNewAspectFillImage(newFrame: CGRect) -> CGRect {
        if let img = self.image {
            let imageRatio = img.size.width / img.size.height
            let fillWidth = newFrame.width / imageRatio
            let size = CGSize(width: newFrame.width, height: fillWidth)
            return CGRect(x: 0, y: 0, width: size.width, height: size.height)
        }
        return .zero
    }
}

// MARK: View
extension UIView {
    
    // Adds a border to the view
    // The design has 0.4, but it didn't look right on the device
    func addBorder() {
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.black.withAlphaComponent(0.2).cgColor
    }
}

// MARK: CGAffineTransform
extension CGAffineTransform {
    
    // Get current scale of transform
    func scale() -> Double {
        return sqrt(Double(self.a * self.a + self.c * self.c))
    }
}
