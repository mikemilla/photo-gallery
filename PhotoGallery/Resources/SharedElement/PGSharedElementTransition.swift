//
//  PGSharedElementTransition.swift
//  PhotoGallery
//
//  Created by Michael Miller on 10/14/17.
//  Copyright © 2017 mikemiller.design. All rights reserved.
//

import UIKit

class PGSharedElementTransition: NSObject, CAAnimationDelegate {
    
    // MARK: Closures
    var onTransitonEnd: (() -> Void)?
    
    // MARK: Animation
    fileprivate let animationGroup = CAAnimationGroup()
    fileprivate var animationPath = UIBezierPath()
    fileprivate let animationLine = CAShapeLayer()
    var shouldDrawAnimationLine = false
    
    // MARK: Required Properties
    var sharedElement:PGSharedElementView!
    var startFrame:CGRect!
    var endFrame:CGRect!
    var animationDuration:TimeInterval!
    var currentRotation:CGFloat = 0
    var flingVelocity:CGFloat = 0
    var isDismissing:Bool = false
    
    // MARK: Init
    init(sharedElement: PGSharedElementView, startFrame: CGRect, endFrame: CGRect, animationDuration: TimeInterval) {
        super.init()
        self.sharedElement = sharedElement
        self.startFrame = startFrame
        self.endFrame = endFrame
        self.animationDuration = animationDuration
    }
    
    // Performs the shared element transition
    func start() {
        
        // Adjust animation for
        sharedElement.contentMode = .scaleAspectFill
        sharedElement.frame.size = isDismissing ? sharedElement.sizeOfImageInView() : startFrame.size
        sharedElement.center = startFrame.calculateCenter()
        sharedElement.addBorder()
        sharedElement.layer.masksToBounds = true
        
        // Create group animation
        // Timing was created using https://matthewlein.com/tools/ceaser
        animationGroup.timingFunction = flingVelocity > 0 || flingVelocity < 0 ? CAMediaTimingFunction(controlPoints: 0.355, 0.870, 0.460, 1.000) : CAMediaTimingFunction(controlPoints: 0.465, 0.010, 0.065, 1.000)
        animationGroup.delegate = self
        animationGroup.duration = animationDuration
        animationGroup.fillMode = kCAFillModeForwards
        animationGroup.isRemovedOnCompletion = false
        
        // Create the animation path
        createAnimationPath(start: startFrame.calculateCenter(), end: endFrame.calculateCenter(), shouldDrawLine: shouldDrawAnimationLine)
        
        // Size Animation
        let sizeAnimation = CAKeyframeAnimation(keyPath: "bounds")
        sizeAnimation.values = [sharedElement.frame, isDismissing ? endFrame : sharedElement.frameForNewAspectFillImage(newFrame: endFrame)]
        
        // Rotation Animation
        let rotationAnimation = CAKeyframeAnimation(keyPath: "transform.rotation")
        rotationAnimation.values = [currentRotation, 0]
        
        // Scale Animation
        let scaleAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
        scaleAnimation.values = [currentRotation != 0 ? sharedElement.transform.scale() : 1, 1]
        
        // Path Position Animation
        let pathAnimation = CAKeyframeAnimation(keyPath: "position")
        pathAnimation.path = animationPath.cgPath
        pathAnimation.values = [startFrame.calculateCenter(), endFrame.calculateCenter()]
        
        // Create animation group
        animationGroup.animations = [sizeAnimation, rotationAnimation, scaleAnimation, pathAnimation]
        
        // Perform Animation
        sharedElement.layer.add(animationGroup, forKey: nil)
    }
    
    // MARK: Core Animation Delegates
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        
        // Handle closure
        if (flag) {
            if let onTransitonEnd = self.onTransitonEnd {
                onTransitonEnd()
            }
        }
        
        // Remove line if it exists
        animationLine.removeFromSuperlayer()
    }
    
    // MARK: Animation Path
    fileprivate func createAnimationPath(start: CGPoint, end: CGPoint, shouldDrawLine: Bool) {
        
        // Draw path
        animationPath = UIBezierPath()
        animationPath.move(to: start)
        
        // Get a proper down/up to left/right "arc"
        // Follows material design are motion principles
        var curvePoint:CGPoint!
        
        // Check interaction and make path feel less jarring
        if (flingVelocity > 0 || flingVelocity < 0) {
            let half = end.x - ((end.x - start.x) / 2)
            curvePoint = CGPoint(x: half, y: start.y + (flingVelocity * 0.02))
        } else if (start.y < end.y) {
            curvePoint = CGPoint(x: start.x, y: end.y)
        } else {
            curvePoint = CGPoint(x: end.x, y: start.y)
        }
        
        // Add curve
        animationPath.addCurve(to: end, controlPoint1: curvePoint, controlPoint2: curvePoint)
        
        // Draw a line
        if (shouldDrawLine) {
            let window = UIApplication.shared.keyWindow!
            animationLine.path = animationPath.cgPath
            animationLine.strokeColor = UIColor.green.cgColor
            animationLine.lineWidth = 4
            animationLine.fillColor = UIColor.clear.cgColor
            animationLine.lineJoin = kCALineJoinRound
            window.layer.addSublayer(animationLine)
        }
    }
}
