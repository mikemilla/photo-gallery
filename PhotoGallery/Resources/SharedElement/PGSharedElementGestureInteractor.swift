//
//  PGSharedElementGestureInteractor.swift
//  PhotoGallery
//
//  Created by Michael Miller on 10/15/17.
//  Copyright © 2017 mikemiller.design. All rights reserved.
//

import UIKit

protocol PGSharedElementGestureInteractorDelegate {
    
    // Pan Gesture
    func didBeginPaging(sharedElement: UIView)
    func didBeginDragging(sharedElement: UIView)
    func isDragging(sharedElement: UIView, translationPosition: CGFloat, distance: CGFloat)
    func didEndDragging(sharedElement: UIView, isDismissable: Bool, velocity: CGFloat)
    
    // Pinch Gesture
    func didBeginPinching(sharedElement: UIView)
    func isPinching(sharedElement: UIView, scale: CGFloat, rotation: CGFloat)
    func didEndPinching(sharedElement: UIView, isDismissable: Bool, scale: CGFloat, rotation: CGFloat)
    
    // Tap Gesture
    func didTap(sharedElement: UIView)
}

class PGSharedElementGestureInteractor: NSObject, UIGestureRecognizerDelegate {
    
    // MARK: Views
    var imageDetailViewController:ImageDetailViewController!
    
    // MARK: Gesture Recognizers
    var pagingGesture: UIPanGestureRecognizer!
    var panGestureWithOneFinger: UIPanGestureRecognizer!
    var pinchGesture: UIPinchGestureRecognizer!
    var panGestureWithTwoFingers: UIPanGestureRecognizer!
    var rotationGesture: UIRotationGestureRecognizer!
    var tapGesture: UITapGestureRecognizer!
    
    // MARK: Thresholds
    fileprivate let dismissActivationThreshold: CGFloat = 10
    fileprivate let dismissDistanceThreshold: CGFloat = 80
    fileprivate let dismissVelocityThreshold: CGFloat = 200
    fileprivate let dismissPinchScaleThreshold: CGFloat = 0.6
    
    // MARK: Shared Element Transform
    fileprivate var sharedElementIdentity: CGAffineTransform = CGAffineTransform.identity
    fileprivate var sharedElementRotation: CGFloat = 0
    
    // MARK: Variables
    var delegate:PGSharedElementGestureInteractorDelegate?
    fileprivate var isAttemptingToDismiss = false
    
    // MARK: Observers
    var isDraggingEnabled:Bool {
        get { return panGestureWithOneFinger.isEnabled }
        set { panGestureWithOneFinger.isEnabled = newValue }
    }
    
    var isPagingEnabled:Bool {
        get { return pagingGesture.isEnabled }
        set { pagingGesture.isEnabled = newValue }
    }
    
    var isPinchingEnabled:Bool {
        get {
            
            // Only returning pinch because all other
            // "pinch" related gestures use pinch gesture
            return pinchGesture.isEnabled
        }
        set {
            pinchGesture.isEnabled = newValue
            panGestureWithTwoFingers.isEnabled = newValue
            rotationGesture.isEnabled = newValue
        }
    }
    
    // MARK: Init
    init(imageDetailViewController: ImageDetailViewController) {
        super.init()
        
        // Save references to the views
        self.imageDetailViewController = imageDetailViewController
        
        // Register the gestures
        setupGestures()
    }
    
    // MARK: Gestures
    fileprivate func setupGestures() {
        
        // Get collection view pan gesture
        pagingGesture = imageDetailViewController.collectionView.panGestureRecognizer
        
        // Pan to dismiss
        panGestureWithOneFinger = UIPanGestureRecognizer(target: self, action: #selector(flickToDismiss(_:)))
        panGestureWithOneFinger.maximumNumberOfTouches = 1
        panGestureWithOneFinger.delegate = self
        imageDetailViewController.collectionView.addGestureRecognizer(panGestureWithOneFinger)
        
        // Pinch to dismiss
        pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(pinchToDismiss(_:)))
        pinchGesture.delegate = self
        imageDetailViewController.collectionView.addGestureRecognizer(pinchGesture)

        // Pinching pan sibling
        panGestureWithTwoFingers = UIPanGestureRecognizer(target: self, action: #selector(panWithPinch(_:)))
        panGestureWithTwoFingers.maximumNumberOfTouches = 2
        panGestureWithTwoFingers.minimumNumberOfTouches = 2
        panGestureWithTwoFingers.delegate = self
        imageDetailViewController.collectionView.addGestureRecognizer(panGestureWithTwoFingers)

        // Rotate pan sibling
        rotationGesture = UIRotationGestureRecognizer(target: self, action: #selector(rotateWithPinch(_:)))
        rotationGesture.delegate = self
        imageDetailViewController.collectionView.addGestureRecognizer(rotationGesture)

        // Tap to clear
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapToToggle(_:)))
        tapGesture.numberOfTapsRequired = 1
        imageDetailViewController.collectionView.addGestureRecognizer(tapGesture)
    }
    
    // MARK: Flick Gesture
    @objc func flickToDismiss(_ panGesture: UIPanGestureRecognizer) {
        
        // Get velocity
        let velocity = panGesture.velocity(in: panGesture.view!.superview)
        
        // Get touch translation distance
        let translation = panGesture.translation(in: panGesture.view!.superview)
        
        // Handle gesture
        switch (panGesture.state) {
        case .began:
            
            delegate?.didBeginDragging(sharedElement: imageDetailViewController.sharedElementView)
            
        case .changed:
            
            // User is attempting to dismiss
            if (isAttemptingToDismiss || translation.y > dismissActivationThreshold || translation.y < -dismissActivationThreshold) {
                
                // Ensure value set
                if (!isAttemptingToDismiss) {
                    delegate?.didBeginDragging(sharedElement: imageDetailViewController.sharedElementView)
                    isAttemptingToDismiss = true
                }
                
                // Disable collection view paging
                if (pagingGesture.isEnabled) {
                    pagingGesture.isEnabled = false
                }

                // Translate the view
                imageDetailViewController.sharedElementView.center = CGPoint(x: imageDetailViewController.sharedElementView.center.x + translation.x, y: imageDetailViewController.sharedElementView.center.y + translation.y)
                panGesture.setTranslation(CGPoint.zero, in: panGesture.view!.superview)
                
                // Call Delegate
                let translationPosition = imageDetailViewController.sharedElementView.center.y - imageDetailViewController.collectionView.center.y
                delegate?.isDragging(sharedElement: imageDetailViewController.sharedElementView,
                                     translationPosition: translationPosition, distance: dismissDistanceThreshold)
                
                // Stop
                // This is here because touches were getting stolen
                // in the following if statement and preventing interaction
                return
            }
            
            // User is attempting to "page" the collectionView
            if (translation.x > dismissActivationThreshold || translation.x < -dismissActivationThreshold) {
                panGestureWithOneFinger.isEnabled = false
                delegate?.didBeginPaging(sharedElement: imageDetailViewController.sharedElementView)
            }
            
        case .ended:
            
            // Check limits
            let thresholdsAreMet = imageDetailViewController.sharedElementView.frame.origin.y > dismissDistanceThreshold || imageDetailViewController.sharedElementView.frame.origin.y < -dismissDistanceThreshold || velocity.y > dismissVelocityThreshold || velocity.y < -dismissDistanceThreshold
            
            // Send to delegate
            delegate?.didEndDragging(sharedElement: imageDetailViewController.sharedElementView,
                                     isDismissable: isAttemptingToDismiss && thresholdsAreMet, velocity: velocity.y)
            
            // Reset values
            enableAllGestures()
            isAttemptingToDismiss = false
            
        default:
            break
        }
    }

    // MARK: Pinch Gestures
    @objc func pinchToDismiss(_ pinchGesture: UIPinchGestureRecognizer) {

        // Prevent zoom in then back out gesture passing
        // This catch allows the user to zoom in then back out on the cell's imageview without causing jank
        // Would like to refactor this ** TODO
        if (imageDetailViewController.sharedElementView.isHidden) {
            if (pinchGesture.scale > 1) {
                return
            }
            if let currentCell = imageDetailViewController.collectionView.cellForItem(at: imageDetailViewController.originalIndexPath) as? ImageDetailCollectionViewCell {
                if (currentCell.scrollView.zoomScale > 1) {
                    return
                }
            }
        }

        // Handle pinches
        switch pinchGesture.state {
        case .began:
            
            sharedElementRotation = 0
            delegate?.didBeginPinching(sharedElement: imageDetailViewController.sharedElementView)

        case .changed:

            // Transform view
            imageDetailViewController.sharedElementView.transform = sharedElementIdentity
                .scaledBy(x: pinchGesture.scale, y: pinchGesture.scale)
                .rotated(by: sharedElementRotation)
            
            // Call delegate
            delegate?.isPinching(sharedElement: imageDetailViewController.sharedElementView,
                                 scale: pinchGesture.scale,
                                 rotation: sharedElementRotation)

        case .ended:
            
            let dismissable = pinchGesture.scale < dismissPinchScaleThreshold
            delegate?.didEndPinching(sharedElement: imageDetailViewController.sharedElementView,
                                     isDismissable: dismissable,
                                     scale: pinchGesture.scale,
                                     rotation: sharedElementRotation)

        default:
            break
        }
    }

    @objc func panWithPinch(_ panGesture: UIPanGestureRecognizer) {

        // Get touch translation distance
        let translation = panGesture.translation(in: panGesture.view!.superview)

        // Translate the view
        imageDetailViewController.sharedElementView.center = CGPoint(x: imageDetailViewController.sharedElementView.center.x + translation.x, y: imageDetailViewController.sharedElementView.center.y + translation.y)
        panGesture.setTranslation(CGPoint.zero, in: imageDetailViewController.sharedElementView.superview)
    }

    @objc func rotateWithPinch(_ rotateGesture: UIRotationGestureRecognizer) {
        sharedElementRotation = rotateGesture.rotation
    }

    // MARK: Tap Gesture
    @objc func tapToToggle(_ tapGesture: UITapGestureRecognizer) {
        delegate?.didTap(sharedElement: imageDetailViewController.sharedElementView)
    }
    
    // MARK: Gesture Recognizer Delegate
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    // MARK: Convenience
    func enableAllGestures() {
        isDraggingEnabled = true
        isPagingEnabled = true
        isPinchingEnabled = true
    }
    
}
