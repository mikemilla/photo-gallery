//
//  PGSharedElementView.swift
//  PhotoGallery
//
//  Created by Michael Miller on 10/15/17.
//  Copyright © 2017 mikemiller.design. All rights reserved.
//

import UIKit

class PGSharedElementView: UIImageView {

    // MARK: Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        // Mask by default
        layer.masksToBounds = true
        
        // Add Shadow
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.26
        layer.shadowOffset = CGSize(width: 0, height: 8)
        layer.shadowRadius = 10
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
